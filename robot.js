const dataRegisters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
	'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
	'AIM', 'SHOT', 'RADAR', 'DAMAGE', 'SPEEDX', 'SPEEDY', 'RANDOM', 'INDEX'];

class Robot {
	constructor(arena, position, code) {
		this.arena = arena;
		this.position = [...position];
		this.velocity = [0, 0];
		this.labels = code[0];
		this.code = code[1];
		this.pc = 0;
		this.acc = 0;
		this.stack = [];
		this.steps = 0;

		this.registers = {};
		this.aim = 0;
		this.radar = 0;
		this.shot = 0;
		this.damage = 100;
		this.speedx = 0;
		this.speedy = 0;
		this.random = null;
		this.index = null;
	}

	scan(angle) {
		angle = (90 - angle) / 180 * Math.PI;
		return -this.arena.scan(this.position, angle);
	}

	shoot(range) {
		if (this.shot > 0) return this.shot;
		if (range >= 13) {
			let angle = (90 - this.aim) / 180 * Math.PI;
			this.arena.shoot(this.position, angle, range);
		}
		return 10;
	}

	getRegister(register) {
		let registers = [
			[/^[A-WZ]$/, () => {                 return this.registers[register];              }],
			[/^[XY]$/  , () => { this.steps = 0; return this.position[{ X:0, Y:1 }[register]]; }],
			[/^AIM$/   , () => { this.steps = 0; return this.aim;                              }],
			[/^RADAR$/ , () => { this.steps = 0; return this.radar;                            }],
			[/^SHOT$/  , () => { this.steps = 0; return this.shot;                             }],
			[/^DAMAGE$/, () => { this.steps = 0; return this.damage;                           }],
			[/^SPEEDX$/, () => { this.steps = 0; return this.speedx;                           }],
			[/^SPEEDY$/, () => { this.steps = 0; return this.speedy;                           }],
			[/^RANDOM$/, () => { this.steps = 0; return this.random * Math.random();           }],
			[/^INDEX$/ , () => {                 return this.index;                            }],
			[/^DATA$/  , () => { return this.getRegister(dataRegisters[this.index]);           }]];
		for (let [pattern, func] of registers)
			if (pattern.test(register)) return func();
	}

	setRegister(register, value) {
		let registers = [
			[/^[A-WZ]$/, (v) => {                 this.registers[register] = v;                   }],
			[/^AIM$/   , (v) => { this.steps = 0; this.aim    = v;                                }],
			[/^RADAR$/ , (v) => { this.steps = 0; this.radar  = this.scan(v);                     }],
			[/^SHOT$/  , (v) => { this.steps = 0; this.shot   = this.shoot(v);                    }],
			[/^SPEEDX$/, (v) => { this.steps = 0; this.speedx = Math.max(-255, Math.min(255, v)); }],
			[/^SPEEDY$/, (v) => { this.steps = 0; this.speedy = Math.max(-255, Math.min(255, v)); }],
			[/^RANDOM$/, (v) => { this.steps = 0; this.random = v;                                }],
			[/^INDEX$/ , (v) => {                 this.index  = v;                                }],
			[/^DATA$/  , (v) => { this.setRegister(dataRegisters[this.index], v);                 }]];
		for (let [pattern, func] of registers)
			if (pattern.test(register)) func(value);
	}

	getData(data) {
		if (data[0] === 'number') {
			return data[1];
		} else if (data[0] == 'register') {
			return this.getRegister(data[1]);
		}
	}

	exec(instr) {
		let arithmetic = { '+': (a, b) => a  +  b, '-': (a, b) => a  -  b, '*': (a, b) => a  *  b, '/': (a, b) => a  /  b };
		let condition  = { '<': (a, b) => a  <  b, '>': (a, b) => a  >  b, '=': (a, b) => a === b, '#': (a, b) => a !== b };
		let instructions = [
			[/^,$|^IF$/ , () => { this.acc = this.getData(instr[1]);                                        }],
			[/^TO$/     , () => { this.setRegister(instr[1], this.acc);                                     }],
			[/^[+\-*/]$/, () => { this.acc = arithmetic[instr[0]](this.acc, this.getData(instr[1]));        }],
			[/^[<>=#]$/ , () => { if (!condition[instr[0]](this.acc, this.getData(instr[1]))) this.pc += 1; }],
			[/^GOTO$/   , () => { this.pc = this.labels[instr[1]];                                          }],
			[/^GOSUB$/  , () => { this.stack.push(this.pc); this.pc = this.labels[instr[1]];                }],
			[/^ENDSUB$/ , () => { this.pc = this.stack.pop();                                               }],
		];
		for (let [pattern, func] of instructions)
			if (pattern.test(instr[0])) func();
	}

	tick() {
		for (this.steps = 10; this.steps-- > 0;)
			this.exec(this.code[this.pc++]);
		if (this.shot > 0) this.shot -= 1;
		this.velocity[0] += Math.max(-40, Math.min(40, this.speedx - this.velocity[0]));
		this.velocity[1] += Math.max(-40, Math.min(40, this.speedy - this.velocity[1]));
		this.position[0] = Math.max(0, Math.min(256, this.position[0] + this.velocity[0] / 50));
		this.position[1] = Math.max(0, Math.min(256, this.position[1] + this.velocity[1] / 50));
		return true;
	}
}