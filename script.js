let cvs = document.getElementById('canvas');
let ctx = cvs.getContext('2d');
ctx.imageSmoothingEnabled = false;

let arena = new Arena();

let file_input = document.getElementById('file');
file_input.addEventListener('change', () => {
	for (let file of file_input.files) {
		let rd = new FileReader();
		rd.addEventListener('load', (e) => {
			arena.load(e.target.result, false);
		});
		rd.readAsText(file, 'UTF-8');
	}
});

setInterval(() => {
	ctx.clearRect(0, 0, cvs.width, cvs.height);
	arena.tick();
	ctx.fillStyle = 'red';
	arena.robots.forEach(robot => {
		ctx.fillRect(robot.position[0] - 10, robot.position[1] + 1, 24, 2);
		ctx.fillRect(robot.position[0] + 1, robot.position[1] - 10, 2, 24);
	});
	ctx.fillStyle = 'blue';
	arena.shells.forEach(shell => {
		ctx.beginPath();
		ctx.arc(shell.position[0], shell.position[1], 3, 0, 2 * Math.PI);
		ctx.fill(); 
	});
}, 1000 / 30);

arena.on('radar', (position, angle) => {
	ctx.beginPath();
	ctx.moveTo(position[0] + 1.5, position[1] + 1.5);
	ctx.lineTo(position[0] + 1.5 + Math.cos(angle) * 500, position[1] + 1.5 - Math.sin(angle) * 500);
	ctx.strokeStyle = 'green';
	ctx.lineWidth = 2;
	ctx.stroke();
});