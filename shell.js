class Shell {
	constructor(arena, position, angle, range) {
		this.arena = arena;
		this.position = [...position];
		this.angle = angle;
		this.range = range;
	}

	tick() {
		let dist = Math.min(100, this.range);
		this.position[0] += Math.cos(this.angle) * dist;
		this.position[1] -= Math.sin(this.angle) * dist;
		this.range -= dist;
		if (this.range > 0) return true;
		this.arena.detonate(this.position);
		return false;
	}
}