class Arena {
	constructor() {
		this.robots = [];
		this.shells = [];
		this.events = {};
	}

	load(code, compiled) {
		let position = [Math.random() * 256, Math.random() * 256];
		if (!compiled) {
			let tokens = tokenize(code);
			console.log(tokens);
			code = parse(tokens, [{}, []]);
			console.log(code);
		}
		this.robots.push(new Robot(this, position, code));
	}

	scan(position, angle) {
		this.trigger('radar', [position, angle]);
		return this.robots.reduce((acc, robot) => {
			let line = [position[0] + 1.5 + Math.cos(angle) * 500, position[1] + 1.5 - Math.sin(angle) * 500];
			let i1 = intersect(position, line, [robot.position[0] - 12, robot.position[1]], [robot.position[0] + 12, robot.position[1]]);
			let i2 = intersect(position, line, [robot.position[0], robot.position[1] - 12], [robot.position[0], robot.position[1] + 12]);
			if (i1 === null && i2 === null) return acc;
			return Math.max(acc, distance(position, robot.position));
		}, -1);
	}

	shoot(position, angle, range) {
		this.shells.push(new Shell(this, position, angle, range));
	}

	detonate(position) {
		this.robots = this.robots.filter(robot => {
			let dist = distance(position, robot.position);
			if (dist > 10) return true;
			robot.damage -= 30 - dist * 3;
			return robot.damage > 0;
		});
	}

	tick() {
		this.robots = this.robots.filter(robot => robot.tick());
		this.shells = this.shells.filter(shell => shell.tick());
	}

	trigger(event, args) {
		(this.events[event] || []).forEach(func => func(...args));
	}

	on(event, func) {
		this.events[event] = (this.events[event] || []);
		this.events[event].push(func);
	}
}