let tokenizers = [
	[/;.*/miy                                                          , $ => ['COMMENT'   , null              ]],
	[/-?\d+\b/miy                                                      , $ => ['NUMBER'    , parseInt($[0])    ]],
	[/TO\b/miy                                                         , $ => ['TO'        , null              ]],
	[/\+|\-|\*|\//miy                                                  , $ => ['ARITHMETIC', $[0]              ]],
	[/<|>|=|#/miy                                                      , $ => ['CONDITION' , $[0]              ]],
	[/IF\b/miy                                                         , $ => ['IF'        , null              ]],
	[/GOTO\b/miy                                                       , $ => ['GOTO'      , null              ]],
	[/GOSUB\b/miy                                                      , $ => ['GOSUB'     , null              ]],
	[/ENDSUB\b/miy                                                     , $ => ['ENDSUB'    , null              ]],
	[/(?:[A-Z]|AIM|SHOT|RADAR|DAMAGE|SPEED[XY]|RANDOM|INDEX|DATA)\b/miy, $ => ['REGISTER'  , $[0].toUpperCase()]],
	[/[A-Z][A-Z0-9]{1,31}\b/miy                                        , $ => ['LABEL'     , $[0].toUpperCase()]],
	[/[\r\n]+/miy                                                      , $ => ['NEWLINE'   , null              ]],
	[/[ \t,]+/miy                                                      , $ => null                              ],
];
let tokenize = (input) => {
	let index = 0;
	let tokens = [];
	while (index < input.length) {
		let failed = tokenizers.every(tokenizer => {
			tokenizer[0].lastIndex = index;
			let match = tokenizer[0].exec(input);
			if (match === null) return true;
			index += match[0].length;
			let result = tokenizer[1](match);
			if (result !== null) tokens.push(result);
		});
		if (failed) {
			console.error(index);
			return null;
		}
	}
	return tokens;
};

let parsers = {
	code: [
		[[['token' , 'LABEL'    ], ['function', ($, c) => { c[0][$[0]] = c[1].length; }], ['parser', 'code2']], ($, c) => null],
		[[                                                                                ['parser', 'code2']], ($, c) => null]],
	code2: [
		[[['token' , 'NEWLINE'  ]                                                       , ['parser', 'code' ]], ($, c) => null],
		[[['token' , 'COMMENT'  ]                                                       , ['parser', 'code2']], ($, c) => null],
		[[['parser', 'statement'], ['function', ($, c) => { c[1].push($[0]);          }], ['parser', 'code2']], ($, c) => null],
		[[['end'                ]                                                                            ], ($, c) => null]],
	statement: [
		[[                         ['parser', 'data'    ]], ($, c) => [','       , $[0]]],
		[[['token', 'TO'        ], ['token' , 'REGISTER']], ($, c) => ['TO'      , $[1]]],
		[[['token', 'ARITHMETIC'], ['parser', 'data'    ]], ($, c) => [$[0]      , $[1]]],
		[[['token', 'CONDITION' ], ['parser', 'data'    ]], ($, c) => [$[0]      , $[1]]],
		[[['token', 'IF'        ], ['parser', 'data'    ]], ($, c) => ['IF'      , $[1]]],
		[[['token', 'GOTO'      ], ['token' , 'LABEL'   ]], ($, c) => ['GOTO'    , $[1]]],
		[[['token', 'GOSUB'     ], ['token' , 'LABEL'   ]], ($, c) => ['GOSUB'   , $[1]]],
		[[['token', 'ENDSUB'    ]                        ], ($, c) => ['ENDSUB'        ]]],
	data: [
		[[['token', 'NUMBER'    ]                        ], ($, c) => ['number'  , $[0]]],
		[[['token', 'REGISTER'  ]                        ], ($, c) => ['register', $[0]]]],
};
let parse = (tokens, ctx) => {
	let frame = ['code', 0, 0, [], null, 0];
	while (true) {
		let parser = parsers[frame[0]];
		if (frame[1] < parser.length) {
			let rules = parser[frame[1]];
			if (frame[2] >= rules[0].length) {
				frame[2] = 0;
				if (frame[4] === null) break;
				frame[4][5] = frame[5];
				frame = frame[4];
				continue;
			}
			let rule = rules[0][frame[2]++];
			if (rule[0] === 'function') {
				frame[3].push(['function', rule[1]]);
				continue;
			} else if (rule[0] === 'end') {
				frame[3].push(['end']);
				if (frame[5] >= tokens.length) continue;
			} else if (rule[0] === 'token') {
				if (frame[5] < tokens.length) {
					frame[3].push(['token', tokens[frame[5]][1]]);
					if (tokens[frame[5]++][0] === rule[1]) continue;
				}
			} else if (rule[0] === 'parser') {
				frame = [rule[1], 0, 0, [], frame, frame[5]];
				frame[4][3].push(['parser', frame]);
				continue;
			}
		} else {
			if (frame[4] === null) return null;
			frame = frame[4];
		}
		frame[1] += 1;
		frame[2] = 0;
		frame[3] = [];
		frame[5] = frame[4] !== null ? frame[4][5] : 0;
	}
	while (true) {
		if (frame[2] >= frame[3].length) {
			if (frame[4] === null) break;
			frame[4][3][frame[4][2]++] = parsers[frame[0]][frame[1]][1](frame[3], ctx);
			frame = frame[4];
			continue;
		}
		let arg = frame[3][frame[2]];
		if (arg[0] === 'function') {
			frame[3][frame[2]++] = arg[1](frame[3], ctx);
		} else if (arg[0] === 'end') {
			frame[3][frame[2]++] = null;
		} else if (arg[0] === 'token') {
			frame[3][frame[2]++] = arg[1];
		} else if (arg[0] === 'parser') {
			frame = arg[1];
		}
	}
	return ctx;
};