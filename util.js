let intersect = (a1, a2, b1, b2) => {
	let ax = a2[0] - a1[0], ay = a2[1] - a1[1];
	let bx = b2[0] - b1[0], by = b2[1] - b1[1];
	let det = by * ax - bx * ay;
	if (det == 0) return null;
	let a = a1[1] - b1[1], b = a1[0] - b1[0];
	let n1 = (bx * a - by * b) / det;
	let n2 = (ax * a - ay * b) / det;
	if (0 < n1 && n1 < 1 && 0 < n2 && n2 < 1) {
		return [a1[0] + n1 * ax, a1[1] + n1 * ay];
	} else return null;
};

let distance2 = (a, b) => {
	return Math.pow(a[0] - b[0], 2) + Math.pow(a[1] - b[1], 2);
};

let distance = (a, b) => {
	return Math.sqrt(distance2(a, b));
};